# Setup
Hello, and welcome to the course! In Setup, we cover the following topics:

Downloading Go
Downloading Visual Studio Code
Hello, World!
Go Packages
Imports
Project Organization

Here are some helpful setup links covered in the lectures.

VSCode - https://code.visualstudio.com/download
Golang Download - https://go.dev/doc/install
Golang Docs - https://go.dev/doc/
Gitlab Repository - https://gitlab.com/blake.roy/coding-bootcamp-the-best-guide-to-go-golang
Contact Email: blake.roy@astronomicstudios.net

We are thrilled to have you in the community, know you will learn lots, and hope you enjoy the journey!