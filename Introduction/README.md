# Introduction
Welcome to the course!

In introduction, we cover what you will learn and how to get help!

Here are some helpful links:

VSCode - https://code.visualstudio.com/download
Golang Download - https://go.dev/doc/install
Golang Docs - https://go.dev/doc/
Gitlab Repository - https://gitlab.com/blake.roy/coding-bootcamp-the-best-guide-to-go-golang
Contact Email: blake.roy@astronomicstudios.net

We are thrilled to have you in the community, know you will learn lots, and hope you enjoy the journey!