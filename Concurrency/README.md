# Concurency
In Concurrency, we cover all of the bells and whistles golang has to offer around concurrent programming. Also, we touch on the topic of parallelism vs concurrency, so please absorb as much from this section as possible!

Topics:
What is concurrency?

Concurrency vs Parallelism

Goroutines
https://gobyexample.com/goroutines

Channels
https://gobyexample.com/channels

Buffered Channels
https://gobyexample.com/channel-buffering

Synchronizing Channels
https://gobyexample.com/channel-synchronization

Waiting on Channel Operations
https://gobyexample.com/select

Timeouts
https://gobyexample.com/timeouts

Closing Channels
https://gobyexample.com/closing-channels

Range Over Channels
https://gobyexample.com/range-over-channels

Timers
https://gobyexample.com/timers

Wait Groups
https://gobyexample.com/waitgroups