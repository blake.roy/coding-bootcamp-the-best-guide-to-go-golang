# Data Structures
In Data Structures, we cover commonly used structures in data to hold and use our data. We also explore structs that allow us to create custom data.

Topics we cover are:

Arrays
https://gobyexample.com/arrays

Slices
https://gobyexample.com/slices

Maps
https://gobyexample.com/maps

Structs
https://gobyexample.com/structs