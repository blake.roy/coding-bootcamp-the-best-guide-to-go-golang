# Golang Basics
In Golang Basics, we cover general programming topics and how to utilize them in Go. Many of these topics are transferrable to other languages, so pay close attention to the theory!

In this section we cover the following topics:

Variables
https://gobyexample.com/variables

Values
https://gobyexample.com/values

Constants
https://gobyexample.com/constants

Loops
https://gobyexample.com/for

Range
https://gobyexample.com/range

Conditional Statements: If-Else
https://gobyexample.com/if-else

Conditional Statements: Switch
https://gobyexample.com/switch

Functions
https://gobyexample.com/functions

Methods
https://gobyexample.com/methods

Multiple Function Return Values
https://gobyexample.com/multiple-return-values

Interfaces
https://gobyexample.com/interfaces

Pointers
https://gobyexample.com/pointers

Generics
https://gobyexample.com/generics

Error Handling
https://gobyexample.com/errors

Writing Files
https://gobyexample.com/writing-files

Reading Files
https://gobyexample.com/reading-files