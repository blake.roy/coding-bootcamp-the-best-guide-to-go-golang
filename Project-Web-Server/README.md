# Web Server

In Project Web Server, we launch a simple HTTP web server that can be accessed on localhost. This project can be expanded upon to build out a RESTful API.

Project Overview

Project Setup

Multiplexers and Handler Functions

Writing the Multiplexer

Adding a Handler Function

Displaying a Message

Conclusion