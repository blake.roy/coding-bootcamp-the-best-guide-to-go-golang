# Testing
In Testing, we cover Go's extensive test tools and how to use them. We also cover how to write different types of tests.

Unit Tests

Table Driven Tests

Running Tests in Parallel

Viewing Test Coverage

Benchmark Tests
https://gobyexample.com/testing-and-benchmarking