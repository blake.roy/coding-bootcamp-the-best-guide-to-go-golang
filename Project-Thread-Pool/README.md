# Thread Pool
In Project Thread Pool, we create a worker pool that is ready to handle a set of tasks concurrently. The goal of this project is to help you understand why we use thread pools, and how we can leverage Go's native concurrency tools to handle multiple tasks.

Project Overview

Project Setup

Thread Pool

Channels

Tasks

Assigning Tasks

Output

Conclusion